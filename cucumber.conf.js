const fs = require("fs");
const path = require("path");
const { setDefaultTimeout, After, AfterAll, BeforeAll } = require("cucumber");
const {
  createSession,
  closeSession,
  startWebDriver,
  stopWebDriver,
} = require("nightwatch-api");
const reporter = require("cucumber-html-reporter");
const { client } = require("nightwatch-api");

const attachedScreenshots = getScreenshots();

function getScreenshots() {
  try {
    const folder = path.resolve(__dirname, "screenshots");
    const screenshots = fs
      .readdirSync(folder)
      .map((file) => path.resolve(folder, file));
    return screenshots;
  } catch (err) {
    return [];
  }
}

setDefaultTimeout(120000);

BeforeAll(async () => {
  await startWebDriver({
    env: process.argv[process.argv.indexOf("--env") + 1],
  });
  await createSession();
  await client.maximizeWindow();
});

AfterAll(async () => {
  let version, browserName, platform;

  //get GOBii server version
  let gobiiVersion;
  await client.getText(
    "body > app > extractor-root > div > div.panel.panel-primary > div.panel-heading > h3",
    (result) => {
      gobiiVersion = result.value.trim();
    }
  );
  gobiiVersion = gobiiVersion.split(" ")[4];

  await client.sessions((result) => {
    browserName = result.value[0]["capabilities"]["browserName"];
    switch (browserName) {
      case "chrome":
        version = result.value[0]["capabilities"]["version"];
        platform = result.value[0]["capabilities"]["platform"];
        break;
      case "firefox":
        version = result.value[0]["capabilities"]["browserVersion"];
        platform = result.value[0]["capabilities"]["platformName"];
        break;
      case "internet explorer":
        platform = result.value[0]["capabilities"]["platformName"];
        version = result.value[0]["capabilities"]["browserVersion"];
        break;
    }
  });

  await closeSession();
  await stopWebDriver();

  setTimeout(() => {
    reporter.generate({
      theme: "bootstrap",
      jsonFile: "report/cucumber_report.json",
      output: "report/cucumber_report.html",
      reportSuiteAsScenarios: true,
      launchReport: true,
      metadata: {
        OS: platform,
        Browser: browserName,
        Version: version,
        "GOBii Extractor Version": gobiiVersion,
      },
    });
  }, 0);
});

After(function () {
  return Promise.all(
    getScreenshots()
      .filter((file) => !attachedScreenshots.includes(file))
      .map((file) => {
        attachedScreenshots.push(file);
        return this.attach(fs.readFileSync(file), "image/png");
      })
  );
});
