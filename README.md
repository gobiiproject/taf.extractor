# TEST AUTOMATION FRAMEWORK
Test Automation Framework provides test automation using 

# Prerequisites:
	- Java 8
	- NodeJS
	- IDE - You can use any IDE that supports NodeJS/JavaScript development. For this document, Visual Studio Code was used

# Visual Studio Code Recommended Extensions:
	- Cucumber (Gherkin) Full Support
	- Cuke Step Definition Generator
	- Gherkin Table Formatter

# Features
Running tests on Chrome, Firefox and Edge (chromium).
Support for Selenium Grid.
Tests written in Gherkin syntax.

# Quick Start
1. Download the repository to target directory.
1. In Visual Studio Code, open the directory.
1. Go to Terminal.
1. Install all the modules.
```sh
npm install
```
2. To run smoke tests with different browsers.
```sh
npm run test -- --env 'chrome' --tags '@smoke'
npm run test -- --env 'firefox' --tags '@smoke'
npm run test -- --env 'edge' --tags '@smoke'
```
3. To run smoke test with different browser in headless mode.
```sh
npm run test -- --env 'headless.chrome' --tags '@smoke'
npm run test -- --env 'headless.firefox' --tags '@smoke'
npm run test -- --env 'headless.edge' --tags '@smoke'
```