const { client } = require("nightwatch-api");
const { Given, Then, When } = require("cucumber");
const gobii = client.page.gobiiPage();
const gobiiHelpers = require("./gobiiHelpers.js");
const { sync } = require("mkdirp");

//await gobii.perform(()=>{debugger;});

Given(/^I login to Gobii as "([^"]*)" "([^"]*)"$/, async (user, password) => {
  await gobiiHelpers.login(user, password);
});

When(/^I select By Dataset$/, async () => {
  await gobiiHelpers.goByDataset();
});

When(/^I select By Samples$/, async () => {
  await gobiiHelpers.goBySamples();
});

When(/^I select By Markers$/, async () => {
  await gobiiHelpers.goByMarkers();
});

When(/^upload file "([^"]*)"$/, async (file) => {
  await gobiiHelpers.setFileUpload(file);
});

When(/^set export format to Flapjack$/, async () => {
  await gobii.click("@flapjackRadioButton");
});

Then(/^title as "([^"]*)"$/, async (title) => {
  await gobii.assert.title(title);
});

Then(/^Job ID contains today's date$/, async () => {
  let dateO = new Date();
  await gobii.assert.containsText(
    "@jobIdText",
    dateO.getFullYear() +
      "_" +
      ("0" + (dateO.getMonth() + 1)).slice(-2) +
      "_" +
      ("0" + dateO.getDate()).slice(-2)
  );
});

When(/^select Dataset Type "([^"]*)"$/, async (dataset) => {
  await gobiiHelpers.setDatasetType(dataset);
});

When(/^select Platform "([^"]*)"$/, async (platform) => {
  await gobiiHelpers.setPlatform(platform);
});

When(/^select Marker Group "([^"]*)"$/, async (markerGroups) => {
  await gobiiHelpers.setMarkerGroup(markerGroups);
});

When(/^select Format "([^"]*)"$/, async (format) => {
  await gobiiHelpers.setFormat(format);
});

When(/^select Mapset "([^"]*)"$/, async (mapset) => {
  await gobiiHelpers.setMapset(mapset);
});

Then(/^I see Extraction Criteria match$/, async (table) => {
  await gobiiHelpers.validateExtractionCriteria(table);
});

When(/^set Extract Filtering$/, async (table) => {
  await gobiiHelpers.setExtractFiltering(table);
});

When(/^select Principal Investigator "([^"]*)"$/, async (principal) => {
  await gobiiHelpers.setPrincipalInvestigator(principal);
});

When(/^select Project "([^"]*)"$/, async (project) => {
  await gobiiHelpers.setProject(project);
});

When(/^select List Item Type "([^"]*)"$/, async (listItemType) => {
  await gobiiHelpers.setListItemType(listItemType);
});

When(/^set list of names "([^"]*)"$/, async (listNames) => {
  await gobiiHelpers.setListName(listNames);
});

When(/^select Experiment "([^"]*)"$/, async (experiment) => {
  await gobiiHelpers.setExperiment(experiment);
});

When(/^select Extract "([^"]*)"$/, async (extracts) => {
  await gobiiHelpers.setExtract(extracts);
});

Then(/^Submit button is green$/, async () => {
  await gobii.assert.attributeContains(
    "@submitButton",
    "class",
    "btn btn-success"
  );
});

Then(/^Submit button is red$/, async () => {
  await gobii.assert.attributeContains(
    "@submitButton",
    "class",
    "btn btn-warning"
  );
});

When(/^select List$/, async () => {
  await gobii.click("@listRadioButton");
});

When(/^a popup 'Marker Groups Already Selected' appears$/, async () => {
  await gobii.assert.containsText(
    "@modalPopupLabel",
    "Marker Groups Already Selected"
  );
});

When(/^select Yes$/, async () => {
  await gobii.click("@yesButton");
});

When(/^List is selected$/, async () => {
  await gobii.assert.attributeContains(
    "@listRadioButton",
    "class",
    "ng-dirty ng-touched"
  );
});

When(/^Marker Group is selected$/, async () => {
  await gobii.assert.attributeContains(
    "@markerGroupsRadioButton",
    "class",
    "ng-dirty ng-touched"
  );
});

When(/^List textbox is empty$/, async () => {
  await gobii.assert.value("@listTextBox", "");
});

When(/^select No$/, async () => {
  await gobii.click("@noButton");
});

When(/^Marker Group "([^"]*)" is checked$/, async (markerGroups) => {
  await gobiiHelpers.markerGroupValidateCheckBox(markerGroups);
});

When(
  /^states 'Marker Groups already selected. Specify Marker List instead\?'$/,
  async () => {
    await gobii.assert.containsText("@modalPopupText", message);
  }
);

When(/^there is an option to select No or Yes$/, async () => {
  await gobii.assert.visible("@yesButton");
  await gobii.assert.visible("@noButton");
});

When(/^in 'Included Markers' File is selected$/, async () => {
  await gobii.assert.attributeContains(
    "@fileRadioButton",
    "class",
    "ng-untouched ng-pristine ng-valid"
  );
});

When(/^in Select Format "([^"]*)" is selected$/, async (format) => {
  switch (format) {
    case "Hapmap":
      await gobii.assert.attributeContains(
        "@hapmapRadioButton",
        "class",
        "ng-untouched ng-pristine ng-valid"
      );
      break;
    case "Flapjack":
      await gobii.assert.attributeContains(
        "@flapjackRadioButton",
        "class",
        "ng-untouched ng-pristine ng-valid"
      );
      break;
    case "Metadata":
      await gobii.assert.attributeContains(
        "@metadataRadioButton",
        "class",
        "ng-untouched ng-pristine ng-valid"
      );
      break;
    default:
      throw new Error(`Unexpected format '${format}' value.`);
  }
});

When(/^Mapset is blank$/, async () => {
  let id;
  await gobii.getValue("@mapsetSelect", (result) => {
    return (id = result.value);
  });

  let element = {
    selector: `option[value='${id}']`,
    locateStrategy: "css selector",
  };

  await gobii.assert.containsText(element, "No Mapset");
});

When(/^Dataset Type is blank$/, async () => {
  let id;
  await gobii.getValue("@datasetTypesSelect", (result) => {
    return (id = result.value);
  });
  let element = {
    selector: `option[value='${id}']`,
    locateStrategy: "css selector",
  };
  gobii.assert.containsText(element, "Select a Dataset Type");
});

When(/^Platform is blank$/, async () => {
  await gobii.assert.not.elementPresent(
    "#CHECKBOX_LIST_Platforms input[type=checkbox]:checked"
  );
});

When(/^File is selected$/, async () => {
  await gobii.assert.attributeEquals(
    {
      selector: "//sample-marker-box//p-radiobutton[1]/div/div[2]",
      locateStrategy: "xpath",
    },
    "class",
    "ui-radiobutton-box ui-widget ui-state-default ui-state-active"
  );
});

When(/^List is not selected$/, async () => {
  await gobii.assert.attributeEquals(
    {
      selector: "//sample-marker-box//p-radiobutton[2]/div/div[2]",
      locateStrategy: "xpath",
    },
    "class",
    "ui-radiobutton-box ui-widget ui-state-default"
  );
});

When(/^Marker Group is not selected$/, async () => {
  await gobii.assert.attributeEquals(
    {
      selector: "//sample-marker-box//p-radiobutton[3]/div/div[2]",
      locateStrategy: "xpath",
    },
    "class",
    "ui-radiobutton-box ui-widget ui-state-default"
  );
});

When(/^Principal Investigator is blank$/, async () => {
  let id;
  await gobii.getValue("@principalInvestigatorSelect", (result) => {
    return (id = result.value);
  });
  await gobii.assert.containsText(
    { selector: `//option[@value="${id}"]`, locateStrategy: "xpath" },
    "All Principal Investigators"
  );
});

When(/^Sample File is blank$/, async () => {
  await gobii.assert.value("@uploadFile", "");
});

Then(/^in Included Samples Germplasm Name is selected$/, async () => {
  await gobii.assert.attributeEquals(
    {
      selector:
        '//*[@id="SAMPLE_LIST_TYPE_RADIO_BUTTON_GERMPLASM_NAME"]/div/div[2]',
      locateStrategy: "xpath",
    },
    "class",
    "ui-radiobutton-box ui-widget ui-state-default ui-state-active"
  );
});

Then(/^Sample List is blank$/, async () => {
  await gobii.assert.not.elementPresent("@listTextBox");
});

Then(/^By Dataset tab is selected$/, async () => {
  await gobii.assert.attributeEquals("@byDatasetTab", "aria-selected", "true");
});

When(/^No dataset selected$/, async () => {
  await gobii.assert.not.elementPresent(
    'p-datatable p-checkbox[ng-reflect-model="true"]'
  );
});

When(/^set list of names to "([^"]*)" entries$/, async (total) => {
  let entries = "";
  for (let index = 0; index < total; index++) {
    gobii.pause(1);
    let time = new Date().getTime();
    entries = entries + time;
    if (index != total - 1) {
      entries = entries + ",";
    }
  }
  await gobiiHelpers.setListName(entries);
});

Then(/^a popup appears with heading "([^"]*)"$/, async (args1) => {
  await gobii.assert.containsText("@exceedListPopupLabel", args1);
});

Then(/^body reads "([^"]*)"$/, async (args1) => {
  await gobii.assert.containsText("@exceedListMessage", args1);
});

When(/^user selects "([^"]*)" row of Datasets$/, async (row) => {
  let selector = {
    selector: `.ui-datatable-data.ui-widget-content tr:nth-child(${row}) .ui-button-secondary`,
    locateStrategy: "css selector",
  };
  await gobii.click(selector);
});

Then(/^a Details popup appears$/, async () => {
  await gobii.expect.section("@datasetDetailsPopup").to.be.visible;
});

When(/^popup Details is "([^"]*)"$/, async (args1) => {
  let popup = gobii.section.datasetDetailsPopup;
  await popup.assert.containsText("@legendText", args1);
});

Then(/^popup Principal Investigator is "([^"]*)"$/, async (args1) => {
  let popup = gobii.section.datasetDetailsPopup;
  await popup.assert.containsText("@principalInvestigatorText", args1);
});

Then(/^popup Loaded By is "([^"]*)"$/, async (args1) => {
  let popup = gobii.section.datasetDetailsPopup;
  await popup.assert.containsText("@loadedByText", args1);
});

Then(/^popup Loaded Date is "([^"]*)"$/, async (args1) => {
  let popup = gobii.section.datasetDetailsPopup;
  return await popup.assert.containsText("@loadedDateText", args1);
});

Then(/^popup Project is "([^"]*)"$/, async (args1) => {
  let popup = gobii.section.datasetDetailsPopup;
  await popup.assert.containsText("@projectText", args1);
});

Then(/^popup Data Type is "([^"]*)"$/, async (args1) => {
  let popup = gobii.section.datasetDetailsPopup;
  await popup.assert.containsText("@dataTypeText", args1);
});

Then(/^popup Calling Analysis is "([^"]*)"$/, async (args1) => {
  let popup = gobii.section.datasetDetailsPopup;
  await popup.assert.containsText("@callingAnalysisText", args1);
});

Then(/^popup Total Samples is "([^"]*)"$/, async (args1) => {
  let popup = gobii.section.datasetDetailsPopup;
  await popup.assert.containsText("@totalSamplesText", args1);
});

Then(/^popup Total Markers is "([^"]*)"$/, async (args1) => {
  let popup = gobii.section.datasetDetailsPopup;
  await popup.assert.containsText("@totalMarkersText", args1);
});

Then(/^popup Experiment is "([^"]*)"$/, async (args1) => {
  let popup = gobii.section.datasetDetailsPopup;
  await popup.assert.containsText("@experimentText", args1);
});

Then(/^popup Platform is "([^"]*)"$/, async (args1) => {
  let popup = gobii.section.datasetDetailsPopup;
  await popup.assert.containsText("@platformText", args1);
});

Then(/^popup Protocol is "([^"]*)"$/, async (args1) => {
  let popup = gobii.section.datasetDetailsPopup;
  await popup.assert.containsText("@protocolText", args1);
});

Then(/^popup Analyses is "([^"]*)"$/, async (args1) => {
  let popup = gobii.section.datasetDetailsPopup;
  await popup.assert.containsText("@analysesText", args1);
});

Then(/^user selects Clear button$/, async () => {
  await gobii.click("@clearButton");
});

Then(/^"([^"]*)" is selected in Principal Investigator$/, async (args1) => {
  await gobii.selectAssertValue("@principalInvestigatorSelect", args1);
});

Then(/^"([^"]*)" is selected in Project$/, async (args1) => {
  await gobii.selectAssertValue("@projectSelect", args1);
});

Then(/^"([^"]*)" is selected in Experiment$/, async (args1) => {
  await gobii.selectAssertValue("@experimentSelect", args1);
});

Then(/^"([^"]*)" is selected Format$/, async (format) => {
  switch (format) {
    case "Hapmap":
      await gobii.assert.attributeContains(
        "@hapmapRadioButton",
        "ng-reflect-model",
        format.toUpperCase()
      );
      break;
    case "Flapjack":
      await gobii.assert.attributeContains(
        "@flapjackRadioButton",
        "ng-reflect-model",
        format.toUpperCase()
      );
      break;
    case "Metadata":
      await gobii.assert.attributeContains(
        "@metadataRadioButton",
        "ng-reflect-model",
        format.toUpperCase()
      );
      break;
    default:
      throw new Error(`Unexpected format '${format}' value.`);
  }
});

Then(/^"([^"]*)" is selected Mapset$/, async (args1) => {
  await gobii.selectAssertValue("@mapsetSelect", args1);
});

Then(/^"([^"]*)" is selected in Dataset Types$/, async (args1) => {
  await gobii.perform(()=>{debugger;});
  await gobii.selectAssertValue("@datasetTypesSelect", args1);
});

Then(/^No Platforms selected$/, async () => {
  await gobii.assert.not.elementPresent(
    "#CHECKBOX_LIST_Platforms input[type=checkbox]:checked"
  );
});

Then(/^"([^"]*)" is selected$/, async (format) => {
  switch (format) {
    case "File":
      await gobii.verify.attributeContains(
        "@fileRadioButton",
        "ng-reflect-model",
        "ITEM_FILE_TYPE"
      );
      break;
    case "List":
      await gobii.verify.attributeContains(
        "@listRadioButton",
        "ng-reflect-model",
        "ITEM_LIST_TYPE"
      );
      break;
    case "Marker Group":
      await gobii.verify.attributeContains(
        "@markerGroupsRadioButton",
        "ng-reflect-model",
        "MARKER_GROUP_TYPE"
      );
      break;
    default:
      throw new Error(`Unexpected format '${format}' value.`);
  }
});

Then(/^"([^"]*)" is selected List Item Type$/, async (args1) => {
  switch (args1) {
    case "Germplasm Name":
      await gobii.verify.attributeContains(
        "@germplasmNameRadioButton",
        "ng-reflect-model",
        "GERMPLASM_NAME"
      );
      break;
    case "External Code":
      await gobii.verify.attributeContains(
        "@externalCodeRadioButton",
        "ng-reflect-model",
        "EXTERNAL_CODE"
      );
      break;
    case "DNA Sample":
      await gobii.verify.attributeContains(
        "@dnaSampleRadioButton",
        "ng-reflect-model",
        "DNA_SAMPLE"
      );
      break;
    default:
      throw new Error(`Unexpected format '${args1}' value.`);
  }
});

Then(/^popup contains$/, async (table) => {
  return await gobiiHelpers.datasetPopupValidate(table);
});

Then(/^No Mapset in Extraction Criteria$/, async() => {
  await gobii.assert.not.containsText("@mapsetText", "Mapset:");
});

Then(/^No Dataset in Extraction Criteria$/, async() => {
	await gobii.assert.not.containsText("@datasetText", "Data Set:");
});

Then(/^No Dataset Type in Extraction Criteria$/, async() => {
	await gobii.assert.not.containsText("@datasetTypeText", "Dataset Type:");
});

Then(/^No Marker Criteria Platform in Extraction Criteria$/, async() => {
  await gobii.assert.not.containsText("@mcPlatformText", "Platform:");
});

Then(/^No Marker File in Extraction Criteria$/, async() => {
  await gobii.assert.not.containsText("@markerFileText", "Marker File:");
});

Then(/^No Marker List in Extraction Criteria$/, async() => {
  await gobii.assert.not.containsText("@mcMarkerListText", "Marker List:");
});

Then(/^No Marker Groups in Extraction Criteria$/, async() => {
  await gobii.assert.not.containsText("@mcMarkerGroupsText", "Maker Groups:");
});

Then(/^No Platform in Extraction Criteria$/, async() => {
  await gobii.assert.not.containsText("@platformText", "Platform:");
});

Then(/^No Principal Investigator in Extraction Criteria$/, async() => {
  await gobii.assert.not.containsText("@scPrincipalInvestigatorText", "Principal Investigator:");
});

Then(/^No Project in Extraction Criteria$/, async() => {
	await gobii.assert.not.containsText("@scProjectText", "Project:");
});

Then(/^No Sample File in Extraction Criteria$/, async() => {
	await gobii.assert.not.containsText("@sampleFileText", "Sample File:");
});

Then(/^No Sample List in Extraction Criteria$/, async() => {
	await gobii.assert.not.containsText("@scSampleListText", "Sample List:");
});





