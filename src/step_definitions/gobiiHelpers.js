const { client } = require("nightwatch-api");
const gobii = client.page.gobiiPage();
//await gobii.perform(()=>{debugger;})

module.exports = {
  login: async function (user, password) {
    await gobii
      .navigate()
      .waitForElementVisible("@agreeCheckBox")
      .click("@agreeCheckBox")
      .setValue("@userNameInputBox", user)
      .setValue("@passwordInputBox", password)
      .click("@loginButton")
      .waitForElementPresent("@submitButton", 60000);
  },

  goByMarkers: async function () {
    await gobii
      .waitForElementVisible("@byMarkersTab", 60000)
      .click("@byMarkersTab")
      .assert.attributeEquals("@byMarkersTab", "aria-selected", "true")
      .pause(1000);
  },

  goBySamples: async function () {
    await gobii
      .waitForElementVisible("@bySamplesTab", 60000)
      .click("@bySamplesTab")
      .assert.attributeEquals("@bySamplesTab", "aria-selected", "true");
  },

  goByDataset: async function () {
    await gobii
      .waitForElementVisible("@byDatasetTab", 60000)
      .click("@byDatasetTab")
      .assert.attributeEquals("@byDatasetTab", "aria-selected", "true");
  },

  setDatasetType: async function (datasetType) {
    await gobii.assert.visible(
      `option[title="${datasetType}"]`,
      datasetType + " is not a valid dataset."
    );
    await gobii.setValue("@datasetTypesSelect", datasetType);
  },

  setPlatform: async function (platforms) {
    await gobii.checkCheckBox(platforms);
  },

  setMapset: async function (mapset) {
    await gobii
      .waitForElementPresent(`option[title="${mapset}"]`)
      .setValue("@mapsetSelect", mapset);
  },

  setFormat: async function (format) {
    if (format == "Hapmap") {
      return await gobii.click("@hapmapRadioButton");
    }
    if (format == "Flapjack") {
      return await gobii.click("@flapjackRadioButton");
    }
    if (format == "Metadata") {
      return await gobii.click("@metadataRadioButton");
    }
    throw new Error(
      "Unexpected format. Supported are 'Hapmap', 'Flapjack', 'Metadata'"
    );
  },

  setMarkerGroup: async function (markerGroup) {
    await gobii.click("@markerGroupsRadioButton");
    for (const mg of markerGroup.split(",")) {
      await gobii
        .waitForElementVisible(`input[type=checkbox][name="${mg.trim()}"]`)
        .click(`input[type=checkbox][name="${mg.trim()}"]`);
    }
  },

  gotoByMarkers: async function () {
    await gobii.goByMarkers();
  },

  setListName: async function (listNames) {
    await gobii.click("@listRadioButton");
    await gobii.setValue(
      "@listTextBox",
      listNames.replace(/,/gi, "\n").replace(/ /gi, "")
    );
    await gobii.click("@listAddToExtractButton");
  },

  setPrincipalInvestigator: async function (principal) {
    await gobii.assert.visible(
      `option[title="${principal}"]`,
      principal + " is not a valid Principal Investigator."
    );
    await gobii.setValue("@principalInvestigatorSelect", principal);
  },

  setProject: async function (project) {
    await gobii.assert.visible(
      `option[title="${project}"]`,
      project + " is not a valid Project."
    );
    await gobii.setValue("@projectSelect", project);
  },

  setListItemType: async function (listItemType) {
    if (listItemType == "Germplasm Name") {
      return await gobii.click("@germplasmNameRadioButton");
    }
    if (listItemType == "External Code") {
      return await gobii.click("@externalCodeRadioButton");
    }
    if (listItemType == "DNA Sample") {
      return await gobii.click("@dnaSampleRadioButton");
    }
    throw new Error(
      "Unexpected format. Supported are 'Germplasm Name', 'External Code', 'DNA Sample'"
    );
  },

  setFileUpload: async function (file) {
    let uploadFile = require("path").resolve("./src/files/" + file);
    if (require("fs").existsSync(uploadFile)) {
      await gobii
        .click("@fileRadioButton")
        .setValue("@uploadFile", uploadFile)
        .click("@fileUploadButton");
    } else {
      throw new Error(`File to upload not found. '${uploadFile}'`);
    }
  },

  setExperiment: async function (experiment) {
    await gobii.setValue("@experimentSelect", experiment);
  },

  setExtract: async function (extracts) {
    await gobii.checkDatatableCheckBox(extracts);
  },

  validateExtractionCriteria: async function (table) {
    for (column of table.raw()[0]) {
      for (hash of table.hashes()) {
        switch (column) {
          case "principalInvestigator":
            await gobii.assert.containsText(
              "@scPrincipalInvestigatorText",
              hash["principalInvestigator"]
            );
            break;
          case "project":
            await gobii.assert.containsText("@scProjectText", hash["project"]);
            break;
          case "format":
            await gobii.assert.containsText("@formatText", hash["format"]);
            break;
          case "mapset":
            await gobii.assert.containsText("@mapsetText", hash["mapset"]);
            break;
          case "datasetType":
            await gobii.assert.containsText(
              "@datasetTypeText",
              hash["datasetType"]
            );
            break;
          case "listItemType":
            await gobii.assert.containsText(
              "@listItemTypeText",
              hash["listItemType"].toUpperCase().replace(" ", "_")
            );
            break;
          case "platform":
            for (platform of hash["platform"].split(",")) {
              await gobii.assert.elementPresent({
                selector: `//*[@id="CRITERIA_TREE"]//span[contains(text(), "Platform: ${platform.trim()}")]`,
                locateStrategy: "xpath",
              });
            }
            break;
          case "markerGroup":
            for (markerGroup of hash["markerGroup"].split(",")) {
              await gobii.assert.elementPresent({
                selector: `//*[@id="CRITERIA_TREE"]//span[contains(text(),"Marker Groups: ${markerGroup.trim()}")]`,
                locateStrategy: "xpath",
              });
            }
            break;
          case "listName":
            for (listName of hash["listName"].split(",")) {
              await gobii.assert.elementPresent({
                selector: `//*[@id="CRITERIA_TREE"]//span[contains(text(), "Sample List: ${listName.trim()}")]`,
                locateStrategy: "xpath",
              });
            }
            break;
          case "file":
            let jobId;
            await gobii.getText("@jobIdText", async (result) => {
              jobId = result.value.split(":")[1].trim();
            });
            try {
              await gobii.waitForElementVisible("@sampleCriteriaSpan");
              await gobii.assert.containsText(
                "@sampleFileText",
                jobId + "_" + "samples.txt"
              );
            } catch {
              await gobii.assert.containsText(
                "@markerFileText",
                jobId + "_" + "markers.txt"
              );
            }
            break;
          case "extract":
            for (extract of hash["extract"].split(",")) {
              await gobii.assert.elementPresent({
                selector: `//*[@id="CRITERIA_TREE"]//span[contains(text(),"Data Set: ${extract.trim()}")]`,
                locateStrategy: "xpath",
              });
            }
            break;
          default:
            throw new Error(`Unexpected ${column} name.`);
        }
      }
    }
  },

  setExtractFiltering: async function (table) {
    for (column of table.raw()[0]) {
      for (hash of table.hashes()) {
        switch (column) {
          case "principalInvestigator":
            await this.setPrincipalInvestigator(hash["principalInvestigator"]);
          case "project":
            await this.setProject(hash["project"]);
            break;
          case "experiment":
            await this.setExperiment(hash["project"]);
            break;
          case "format":
            await this.setFormat(hash["format"]);
            break;
          case "mapset":
            await this.setMapset(hash["mapset"]);
            break;
          case "datasetType":
            await this.setDatasetType(hash["datasetType"]);
            break;
          case "platform":
            await this.setPlatform(hash["platform"]);
            break;
          case "markerGroup":
            await this.setMarkerGroup(hash["markerGroup"]);
            break;
          case "listItemType":
            await this.setListItemType(hash["listItemType"]);
            break;
          case "listName":
            await this.setListName(hash["listName"]);
            break;
          case "file":
            await this.setFileUpload(hash["file"]);
            break;
          case "extract":
            await this.setExtract(hash["extract"]);
            break;
          default:
            throw new Error(`Unexpected column '${column}' name.`);
        }
      }
    }
  },

  markerGroupValidateCheckBox: async function (markerGroups) {
    for (markerGroup of markerGroups.split(",")) {
      gobii.assert.domPropertyEquals(
        `input[type=checkbox][name="${markerGroup.trim()}"]`,
        "checked",
        true
      );
    }
    return;
  },

  datasetPopupFieldValidate: async function (control, expectedText) {
    let popup = gobii.section.datasetDetailsPopup;
    return await popup.assert.containsText(control, expectedText);
  },

  datasetPopupValidate: async function (table) {
    for (column of table.raw()[0]) {
      for (hash of table.hashes()) {
        switch (column) {
          case "details":
            await this.datasetPopupFieldValidate(
              "@legendText",
              hash["details"]
            );
            break;
          case "principalInvestigator":
            await this.datasetPopupFieldValidate(
              "@principalInvestigatorText",
              hash["principalInvestigator"]
            );
            break;
          case "loadedBy":
            await this.datasetPopupFieldValidate(
              "@loadedByText",
              hash["loadedBy"]
            );
            break;
          case "loadedDate":
            await this.datasetPopupFieldValidate(
              "@loadedDateText",
              hash["loadedDate"]
            );
            break;
          case "project":
            await this.datasetPopupFieldValidate(
              "@projectText",
              hash["project"]
            );
            break;
          case "dataType":
            await this.datasetPopupFieldValidate(
              "@dataTypeText",
              hash["dataType"]
            );
            break;
          case "callingAnalysis":
            await this.datasetPopupFieldValidate(
              "@callingAnalysisText",
              hash["callingAnalysis"]
            );
            break;
          case "totalSamples":
            await this.datasetPopupFieldValidate(
              "@totalSamplesText",
              hash["totalSamples"]
            );
            break;
          case "totalMarkers":
            await this.datasetPopupFieldValidate(
              "@totalMarkersText",
              hash["totalMarkers"]
            );
            break;
          case "experiment":
            await this.datasetPopupFieldValidate(
              "@experimentText",
              hash["experiment"]
            );
            break;
          case "platform":
            await this.datasetPopupFieldValidate(
              "@platformText",
              hash["platform"]
            );
            break;
          case "protocol":
            await this.datasetPopupFieldValidate(
              "@protocolText",
              hash["protocol"]
            );
            break;
          case "analyses":
            await this.datasetPopupFieldValidate(
              "@analysesText",
              hash["analyses"]
            );
            break;
          default:
            throw new Error(`Unexpected ${column} name.`);
        }
      }
    }
  },
};
