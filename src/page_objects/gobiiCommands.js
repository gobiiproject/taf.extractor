module.exports = {
  commands: {
    checkCheckBox: function (options) {
      if (!options.includes(",")) {
        let selector = `input[type=checkbox][name="${options.trim()}"]`;
        return this.waitForElementVisible(selector).click(selector);
      }
      for (const option of options.split(",")) {
        let selector = `input[type=checkbox][name="${option.trim()}"]`;
        this.waitForElementVisible(selector).click(selector);
      }
    },

    checkDatatableCheckBox: function (options) {
      if (!options.includes(",")) {
        let selector = {
          selector: `//span[contains(text(), '${options.trim()}')]/ancestor::tr//p-checkbox`,
          locateStrategy: "xpath",
        };
        return this.waitForElementVisible(selector).click(selector);
      }
      for (const option of options.split(",")) {
        let selector = {
          selector: `//span[contains(text(), '${option.trim()}')]/ancestor::tr//p-checkbox`,
          locateStrategy: "xpath",
        };
        this.waitForElementVisible(selector).click(selector);
      }
      return this;
    },

    selectAssertValue: async function (control, value) {
      let id;
      await this.getValue(control, (result) => {
        return (id = result.value);
      });
      let selector = {
        selector: `//option[@value="${id}"]`,
        locateStrategy: "xpath",
      };
      await this.assert.containsText(selector, value);
      return this;
    },
  },
};
