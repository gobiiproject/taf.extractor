const commands = require("./gobiiCommands.js");

module.exports = {
  url: "http://cbsugobiixvm10.biohpc.cornell.edu:8081/gobii-dev/login",
  commands: [commands.commands],
  elements: {
    //login
    agreeCheckBox: {
      selector: "#LOGIN_AGREE_TO_TERMS_CHECKBOX",
      timeout: 30000,
    },
    userNameInputBox: "#LOGIN_USER_NAME_INPUT",
    passwordInputBox: "#LOGIN_PASSWORD_INPUT",
    loginButton: "#LOGIN_SUBMIT_BUTTON",

    //tabs
    byDatasetTab: "#ui-tabpanel-0-label",
    bySamplesTab: "#ui-tabpanel-1-label",
    byMarkersTab: "#ui-tabpanel-2-label",

    //By Dataset
    principalInvestigatorSelect: {
      selector: '//select[contains(@id, "NAME_ID_LIST_Contact-P")]',
      locateStrategy: "xpath",
    },
    projectSelect: {
      selector: '//select[contains(@id, "NAME_ID_LIST_Project")]',
      locateStrategy: "xpath",
    },
    experimentSelect: "#NAME_ID_LIST_Experiment-Filter-Optional",
    extractReadyCheckBox:
      "#ui-tabpanel-0 > div > div > div > div > div.panel-body > dataset-datatable > div > p-checkbox",

    //By Markers
    datasetTypesSelect: "#NAME_ID_LIST_Cv-DataType",

    //Platform Combobox
    affymetrixAxiomCombobox: 'input[type=checkbox][name="Affymetrix_Axiom"]',
    dartAmpliconCombobox: 'input[type=checkbox][name="Dart_Amplicon"]',
    dartcloneCombobox: 'input[type=checkbox][name="Dart_clone"]',
    dartseqsnpsCombobox: 'input[type=checkbox][name="Dartseq_snps"]',
    illuminaGoldengateCombobox:
      'input[type=checkbox][name="Illumina_Goldengate"]',
    illuminaInfiniumCombobox: 'input[type=checkbox][name="Illumina_Infinium"]',
    iIlluminaTSCACombobox: 'input[type=checkbox][name="Illumina_TSCA"]',
    inDelsCombobox: 'input[type=checkbox][name="InDels"]',
    kaspCombobox: 'input[type=checkbox][name="KASP"]',
    platformLesliCombobox:
      'input[type=checkbox][name="platform_lts_0001_SSR_codom_1-10__Lesli"]',
    sequencingCombobox: 'input[type=checkbox][name="Sequencing"]',
    ssrSTSCAPSCombobox: 'input[type=checkbox][name="SSR_STS_CAPS"]',
    noneCombobox: 'input[type=checkbox][name="<none>"]',

    //Included Samples
    germplasmNameRadioButton: "#SAMPLE_LIST_TYPE_RADIO_BUTTON_GERMPLASM_NAME",
    externalCodeRadioButton: "#SAMPLE_LIST_TYPE_RADIO_BUTTON_EXTERNAL_CODE",
    dnaSampleRadioButton: "SAMPLE_LIST_TYPE_RADIO_BUTTON_DNA_SAMPLE",

    //included
    fileRadioButton: {
      selector: "//sample-marker-box//p-radiobutton[1]",
      locateStrategy: "xpath",
    },
    listRadioButton: {
      selector: "//sample-marker-box//p-radiobutton[2]",
      locateStrategy: "xpath",
    },
    markerGroupsRadioButton: {
      selector: "//sample-marker-box//p-radiobutton[3]",
      locateStrategy: "xpath",
    },

    uploadFile: "#FILE_SELECTOR_MARKER_SAMPLE_LIST_UPLOAD",
    fileUploadButton: "#SUBMIT_BUTTON_UPLOAD_MARKER_SAMPLE_LIST",

    //list
    listAddToExtractButton: "text-area > button.btn.btn-success",
    listTextBox: "text-area > textarea",
    listClearButton: "text-area > button.btn.btn-primary",

    //export format
    hapmapRadioButton: "#EXPORT_FORMAT_RADIO_BUTTON_HAPMAP",
    flapjackRadioButton: "#EXPORT_FORMAT_RADIO_BUTTON_FLAPJACK",
    metadataRadioButton: "#EXPORT_FORMAT_RADIO_BUTTON_META_DATA_ONLY",
    mapsetSelect: "#NAME_ID_LIST_Mapsets",

    //extraction criteria
    jobIdText: {
      selector: '//*[@id="CRITERIA_TREE"]//span[contains(text(),"Job ID")]',
      locateStrategy: "xpath",
    },
    submitAsText: {
      selector: '//*[@id="CRITERIA_TREE"]//span[contains(text(),"Submit As")]',
      locateStrategy: "xpath",
    },
    formatText: {
      selector: '//*[@id="CRITERIA_TREE"]//span[contains(text(),"Format")]',
      locateStrategy: "xpath",
    },
    mapsetText: {
      selector: '//*[@id="CRITERIA_TREE"]//span[contains(text(),"Mapset")]',
      locateStrategy: "xpath",
    },
    datasetText: {
      selector:
        '//*[@id="CRITERIA_TREE"]//span[contains(text(),"Data Set")]',
      locateStrategy: "xpath",
    },
    datasetTypeText: {
      selector:
        '//*[@id="CRITERIA_TREE"]//span[contains(text(),"Dataset Type")]',
      locateStrategy: "xpath",
    },
    listItemTypeText: {
      selector: '//*[@id="CRITERIA_TREE"]//span[contains(text(),"List Type")]',
      locateStrategy: "xpath",
    },
    platformText: {
      selector: '//*[@id="CRITERIA_TREE"]//span[contains(text(),"Platform")]',
      locateStrategy: "xpath",
    },
    scPrincipalInvestigatorText: {
      selector:
        '//*[@id="CRITERIA_TREE"]//span[contains(text(),"Principal Investigator")]',
      locateStrategy: "xpath",
    },
    scProjectText: {
      selector: '//*[@id="CRITERIA_TREE"]//span[contains(text(),"Project")]',
      locateStrategy: "xpath",
    },
    sampleCriteriaSpan: {
      selector:
        '//*[@id="CRITERIA_TREE"]//span[contains(text(), "Samples Criteria")]',
      locateStrategy: "xpath",
    },
    sampleFileText: {
      selector:
        '//*[@id="CRITERIA_TREE"]//span[contains(text(),"Sample File")]',
      locateStrategy: "xpath",
    },
    markerFileText: {
      selector:
        '//*[@id="CRITERIA_TREE"]//span[contains(text(),"Marker File")]',
      locateStrategy: "xpath",
    },
    scSampleListText: {
      selector:
        '//*[@id="CRITERIA_TREE"]//span[contains(text(),"Sample List")]',
      locateStrategy: "xpath",
    },
    mcPlatformText: {
      selector: '//*[@id="CRITERIA_TREE"]//span[contains(text(),"Platform")]',
      locateStrategy: "xpath",
    },
    markerFileText: {
      selector:
        '//*[@id="CRITERIA_TREE"]//span[contains(text(),"Marker File")]',
      locateStrategy: "xpath",
    },
    mcMarkerListText: {
      selector:
        '//*[@id="CRITERIA_TREE"]//span[contains(text(),"Marker List")]',
      locateStrategy: "xpath",
    },
    mcMarkerGroupsText: {
      selector:
        '//*[@id="CRITERIA_TREE"]//span[contains(text(),"Marker Groups")]',
      locateStrategy: "xpath",
    },
    submitButton: "#SUBMIT_BUTTON_EXTRACT",
    clearButton: "div.col-md-4 div.panel.panel-primary button.btn.btn-primary",

    //marker group already selected modal
    modalPopupLabel: "#ui-dialog-1-label",
    yesButton:
      "p-footer > button:nth-child(2) > span.ui-button-text.ui-clickable",
    noButton:
      "p-footer > button:nth-child(1) > span.ui-button-text.ui-clickable",
    modalPopupText: ".ui-dialog-content > p:nth-child(1)",

    //exceed list names
    exceedListPopupLabel: "#ui-dialog-2-label",
    exceedListMessage:
      "p-dialog.ng-tns-c3-5 .ui-dialog-content.ui-widget-content",
  },
  //Dataset popup
  sections: {
    datasetDetailsPopup: {
      selector: ".ui-overlaypanel-content",
      elements: {
        legendText: "legend",
        principalInvestigatorText: {
          selector:
            '//b[contains(text(), "Principal Investigator")]/../following-sibling::td',
          locateStrategy: "xpath",
        },
        loadedByText: {
          selector:
            '//b[contains(text(), "Loaded By")]/../following-sibling::td',
          locateStrategy: "xpath",
        },
        loadedDateText: {
          selector:
            '//b[contains(text(), "Loaded Date")]/../following-sibling::td',
          locateStrategy: "xpath",
        },
        projectText: {
          selector: '//b[contains(text(), "Project")]/../following-sibling::td',
          locateStrategy: "xpath",
        },
        dataTypeText: {
          selector:
            '//b[contains(text(), "Data Type")]/../following-sibling::td',
          locateStrategy: "xpath",
        },
        callingAnalysisText: {
          selector:
            '//b[contains(text(), "Calling Analysis")]/../following-sibling::td',
          locateStrategy: "xpath",
        },
        totalSamplesText: {
          selector:
            '//b[contains(text(), "Total Samples")]/../following-sibling::td',
          locateStrategy: "xpath",
        },
        totalMarkersText: {
          selector:
            '//b[contains(text(), "Total Markers")]/../following-sibling::td',
          locateStrategy: "xpath",
        },
        experimentText: {
          selector:
            '//b[contains(text(), "Experiment")]/../following-sibling::td',
          locateStrategy: "xpath",
        },
        platformText: {
          selector:
            '//b[contains(text(), "Platform")]/../following-sibling::td',
          locateStrategy: "xpath",
        },
        protocolText: {
          selector:
            '//b[contains(text(), "Protocol")]/../following-sibling::td',
          locateStrategy: "xpath",
        },
        analysesText: "span.ui-panel-title.ng-tns-c0-3.ng-star-inserted",
      },
    },
  },
};
