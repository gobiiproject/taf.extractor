@debug
Feature: By Samples Test

        Background: Login User
            Given I login to Gobii as "cvacalares" "g0b11"
              And  I select By Samples
     
        @GE-109
        Scenario Outline: By Markers Test Verification Table
              And select Principal Investigator "<principalInvestigator>"
              And select Project "<project>"
              And select Dataset Type "<datasetType>"
              And select Platform "<platform>"
              And select List Item Type "<listItemType>"
              And set list of names "<listName>"
              And select Format "<format>"
             Then I see Extraction Criteria match
                  | principalInvestigator   | project   | datasetType   | platform   | listItemType   | listName   | format   |
                  | <principalInvestigator> | <project> | <datasetType> | <platform> | <listItemType> | <listName> | <format> |

        Examples:
                  | principalInvestigator | project        | datasetType         | platform | listItemType   | listName                                                                                                                         | format   |
                  | Weigand, Deb          | hotfix_2.2.1-9 | nucleotide_2_letter | KASP     | Germplasm Name | germplasmname_Deb_2nuc_1, germplasmname_Deb_2nuc_2, germplasmname_Deb_2nuc_3, germplasmname_Deb_2nuc_4, germplasmname_Deb_2nuc_5 | Flapjack |
                  
        @GE-109
        Scenario Outline: By Markers Test Setup Table
              And set Extract Filtering
                  | principalInvestigator   | project   | datasetType   | platform   | listItemType   | listName   | format   |
                  | <principalInvestigator> | <project> | <datasetType> | <platform> | <listItemType> | <listName> | <format> |
             Then I see Extraction Criteria match
                  | principalInvestigator   | project   | datasetType   | platform   | listItemType   | listName   | format   |
                  | <principalInvestigator> | <project> | <datasetType> | <platform> | <listItemType> | <listName> | <format> |
               
        Examples:
                  | principalInvestigator | project        | datasetType         | platform | listItemType   | listName                                                                                                                         | format   |
                  | Weigand, Deb          | hotfix_2.2.1-9 | nucleotide_2_letter | KASP     | Germplasm Name | germplasmname_Deb_2nuc_1, germplasmname_Deb_2nuc_2, germplasmname_Deb_2nuc_3, germplasmname_Deb_2nuc_4, germplasmname_Deb_2nuc_5 | Flapjack |
               
        @GE-110
        Scenario Outline: Provide Extract by Sample Sample File
              And set Extract Filtering
                  | principalInvestigator   | project   | datasetType   | file   | format   |
                  | <principalInvestigator> | <project> | <datasetType> | <file> | <format> |
             Then I see Extraction Criteria match
                  | principalInvestigator   | project   | datasetType   | file   | format   |
                  | <principalInvestigator> | <project> | <datasetType> | <file> | <format> |

        Examples:
                  | principalInvestigator | project        | datasetType         | file            | format   |
                  | Weigand, Deb          | hotfix_2.2.1-9 | nucleotide_2_letter | sample_list.txt | Flapjack |

        @GE-172
        Scenario: Gherkin-BySample_default
             Then "All Principal Investigator" is selected in Principal Investigator
              And "Select a Dataset Type" is selected in Dataset Types
              And "All Projects" is selected in Project
              And No Platforms selected
              And "Hapmap" is selected Format
              And "No Mapset" is selected Mapset
              And "Germplasm Name" is selected List Item Type
              And I see Extraction Criteria match
                  | format | listItemType   |
                  | Hapmap | Germplasm Name |
              And No Mapset in Extraction Criteria
              And No Dataset Type in Extraction Criteria
              And No Platform in Extraction Criteria
              And No Principal Investigator in Extraction Criteria
              And No Project in Extraction Criteria
              And No Sample File in Extraction Criteria
              And No Sample List in Extraction Criteria
              And Submit button is red

        @GE-204
        Scenario: Gherkin-bySamples_listLimit 201
              And set list of names to "201" entries
             Then a popup appears with heading "Maximum Sample List Items Exceeded"
              And body reads "You attempted to paste more than 200 Sample List items; Please reduce the size of the list"

        @GE-212
        Scenario: Gherkin-bySamples_listLimit 200
              And select Dataset Type "co_dominant_non_nucleotide"
              And set list of names to "200" entries
             Then Submit button is green
    
        @GE-178
        Scenario: bySamples_minimumSelections
              And select Principal Investigator "Weigand, Deb"
             Then Submit button is red
             When select Project "hotfix_2.2.1-9"
             Then Submit button is red
             When select Platform "KASP"
             Then Submit button is red
             When select Dataset Type "nucleotide_2_letter"
             Then Submit button is green
        
        Scenario: Clicking Clear Button resets fields to default values
              And set Extract Filtering
                  | principalInvestigator | project        | datasetType         | platform | listItemType | listName                 | mapset  | format   |
                  | Weigand, Deb          | hotfix_2.2.1-9 | nucleotide_2_letter | KASP     | DNA Sample   | germplasmname_Deb_2nuc_1 | devtest | Flapjack |
              And user selects Clear button
             Then "All Principal Investigator" is selected in Principal Investigator
              And "Select a Dataset Type" is selected in Dataset Types
              And "All Projects" is selected in Project
              And No Platforms selected
              And "Hapmap" is selected Format
              And "No Mapset" is selected Mapset
              And "Germplasm Name" is selected List Item Type
              And I see Extraction Criteria match
                  | format | listItemType   |
                  | Hapmap | Germplasm Name |
              And No Mapset in Extraction Criteria
              And No Dataset Type in Extraction Criteria
              And No Platform in Extraction Criteria
              And No Principal Investigator in Extraction Criteria
              And No Project in Extraction Criteria
              And No Sample File in Extraction Criteria
              And No Sample List in Extraction Criteria
              And Submit button is red

        Scenario: Switching tabs resets fields to default values
              And set Extract Filtering
                  | principalInvestigator | project        | datasetType         | platform | listItemType | listName                 | format   | mapset  |
                  | Weigand, Deb          | hotfix_2.2.1-9 | nucleotide_2_letter | KASP     | DNA Sample   | germplasmname_Deb_2nuc_1 | Flapjack | devtest |
              And I select By Dataset
              And I select By Samples
             Then "All Principal Investigator" is selected in Principal Investigator
              And "Select a Dataset Type" is selected in Dataset Types
              And "All Projects" is selected in Project
              And No Platforms selected
              And "Hapmap" is selected Format
              And "No Mapset" is selected Mapset
              And "Germplasm Name" is selected List Item Type
              And I see Extraction Criteria match
                  | format | listItemType   |
                  | Hapmap | Germplasm Name |
              And No Mapset in Extraction Criteria
              And No Dataset Type in Extraction Criteria
              And No Platform in Extraction Criteria
              And No Principal Investigator in Extraction Criteria
              And No Project in Extraction Criteria
              And No Sample File in Extraction Criteria
              And No Sample List in Extraction Criteria
              And Submit button is red