Feature: By Dataset Test
        Background: Login User
            Given I login to Gobii as "cvacalares" "g0b11"
             When I select By Dataset

        Scenario Outline: By Dataset Test Verification Table
              And select Principal Investigator "<principalInvestigator>"
              And select Project "<project>"
              And select Experiment "<experiment>"
              And select Extract "<extract>"
              And select Format "<format>"
             Then I see Extraction Criteria match
                  | extract   | format   |
                  | <extract> | <format> |

        Examples:
                  | principalInvestigator | project    | experiment | extract                                                    | format   |
                  | Weigand, Deb          | 4ltr_xvm10 | 4ltr_xvm10 | 2ltr_xvm10_KASP_GSD-166_DS_1, 2ltr_xvm10_KASP_GSD-166_DS_2 | Flapjack |

        Scenario Outline: By Dataset Test Setup Table
              And set Extract Filtering
                  | principalInvestigator | project    | experiment | extract                                                    | format   |
                  | Weigand, Deb          | 4ltr_xvm10 | 4ltr_xvm10 | 2ltr_xvm10_KASP_GSD-166_DS_1, 2ltr_xvm10_KASP_GSD-166_DS_2 | Flapjack |
             Then I see Extraction Criteria match
                  | extract   | format   |
                  | <extract> | <format> |

        Examples:
                  | principalInvestigator | project    | experiment | extract                                                    | format   |
                  | Weigand, Deb          | 4ltr_xvm10 | 4ltr_xvm10 | 2ltr_xvm10_KASP_GSD-166_DS_1, 2ltr_xvm10_KASP_GSD-166_DS_2 | Flapjack |

    
        #Validate when landing on By Dataset, the values are on default
        @GE-171
        Scenario: Gherkin-byDataset_default
             Then I see Extraction Criteria match
                  | format |
                  | Hapmap |
             Then "All Principal Investigator" is selected in Principal Investigator
              And "All Projects" is selected in Project
              And "All Experiments" is selected in Experiment
              And No dataset selected
              And "Hapmap" is selected Format
              And "No Mapset" is selected Mapset
              And No Mapset in Extraction Criteria
              And No Dataset in Extraction Criteria
              And Submit button is red

        @GE-182
        Scenario: By Dataset Details
              And select Principal Investigator "Jones, Liz"
              And select Project "rice_pedver"
              And user selects "1" row of Datasets
             Then a Details popup appears
              And popup Details is "rice_pedver"
              And popup Principal Investigator is "Jones, Liz"
              And popup Loaded By is "Jones, Liz"
              And popup Loaded Date is "2020-03-21"
              And popup Project is "rice_pedver"
              And popup Data Type is "nucleotide_2_letter"
              And popup Calling Analysis is "Test_Calling"
              And popup Total Samples is "106"
              And popup Total Markers is "9"
              And popup Experiment is "rice_pedver"
              And popup Platform is "KASP"
              And popup Protocol is "KASP"
              And popup Analyses is "0"

        @GE-182
        Scenario: By Dataset Details Table
              And set Extract Filtering
                  | principalInvestigator | project     |
                  | Jones, Liz            | rice_pedver |
              And user selects "1" row of Datasets
             Then a Details popup appears
              And popup contains
                  | details     | principalInvestigator | loadedBy   | loadedDate | project     | dataType            | callingAnalysis | totalSamples | totalMarkers | experiment  | platform | protocol | analyses |
                  | rice_pedver | Jones, Liz            | Jones, Liz | 2020-03-21 | rice_pedver | nucleotide_2_letter | Test_Calling    | 106          | 9            | rice_pedver | KASP     | KASP     | 0        |

        Scenario: Clicking Clear Button resets fields to default values
              And set Extract Filtering
                  | principalInvestigator | project    | experiment | extract                                                    | format   | mapset  |
                  | Weigand, Deb          | 4ltr_xvm10 | 4ltr_xvm10 | 2ltr_xvm10_KASP_GSD-166_DS_1, 2ltr_xvm10_KASP_GSD-166_DS_2 | Flapjack | devtest |
              And user selects Clear button
             Then "All Principal Investigator" is selected in Principal Investigator
              And "All Projects" is selected in Project
              And "All Experiments" is selected in Experiment
              And No dataset selected
              And "Hapmap" is selected Format
              And "No Mapset" is selected Mapset
              And No Mapset in Extraction Criteria
              And No Dataset in Extraction Criteria
              And Submit button is red

        Scenario: Switching tabs resets fields to default values
              And set Extract Filtering
                  | principalInvestigator | project    | experiment | extract                                                    | format   | mapset  |
                  | Weigand, Deb          | 4ltr_xvm10 | 4ltr_xvm10 | 2ltr_xvm10_KASP_GSD-166_DS_1, 2ltr_xvm10_KASP_GSD-166_DS_2 | Flapjack | devtest |
              And I select By Markers
              And I select By Dataset
             Then "All Principal Investigator" is selected in Principal Investigator
              And "All Projects" is selected in Project
              And "All Experiments" is selected in Experiment
              And No dataset selected
              And "Hapmap" is selected Format
              And "No Mapset" is selected Mapset
              And No Mapset in Extraction Criteria
              And No Dataset in Extraction Criteria
              And Submit button is red
      