Feature: By Markers Test

        Background: Login User
            Given I login to Gobii as "cvacalares" "g0b11"
              And I select By Markers

        @GE-108
        Scenario Outline: By Markers Test Verification Table
              And select Dataset Type "<datasetType>"
              And select Platform "<platform>"
              And select Marker Group "<markerGroup>"
              And select Mapset "<mapset>"
              And select Format "<format>"
             Then I see Extraction Criteria match
                  | markerGroup   | format   | mapset   |
                  | <markerGroup> | <format> | <mapset> |
                  
        Examples:
                  | datasetType                | platform                        | markerGroup                      | format | mapset  |
                  | co_dominant_non_nucleotide | Affymetrix_Axiom, Dart_Amplicon | Affy_dom_mg_1-25, Affy_dom_mg_v2 | Hapmap | devtest |

        @GE-108
        Scenario Outline: By Markers Test Table Setup
              And set Extract Filtering
                  | datasetType               | platform    | markerGroup   | format    | mapset    |
                  |<datasetType>              |<platform>   | <markerGroup> | <format>  | <mapset>  |
             Then I see Extraction Criteria match
                  | datasetType   | platform   | markerGroup   | format   | mapset   |
                  | <datasetType> | <platform> | <markerGroup> | <format> | <mapset> |
                    
        Examples:
                  | datasetType                | platform                        | markerGroup                      | format | mapset  |
                  | co_dominant_non_nucleotide | Affymetrix_Axiom, Dart_Amplicon | Affy_dom_mg_1-25, Affy_dom_mg_v2 | Hapmap | devtest |

        @GE-110
        Scenario Outline: Provide Extract by Marker Sample File
              And set Extract Filtering
                  | datasetType   | platform   | file   | format   |
                  | <datasetType> | <platform> | <file> | <format> |
             Then I see Extraction Criteria match
                  | datasetType   | platform   | file   | format   |
                  | <datasetType> | <platform> | <file> | <format> |
                  
        Examples:
                  | datasetType | platform   | file            | format   |
                  | iupac       | Dart_clone | sample_list.txt | Flapjack |

        @GE-173
        Scenario: Gherkin-byMarker_default
             Then "Select a Dataset Type" is selected in Dataset Types
              And No Platforms selected
              And "Hapmap" is selected Format
              And "No Mapset" is selected Mapset
              And "Hapmap" is selected Format
             Then I see Extraction Criteria match
                  | format |
                  | Hapmap |
              And No Mapset in Extraction Criteria
              And No Dataset Type in Extraction Criteria
              And No Marker Criteria Platform in Extraction Criteria
              And No Marker File in Extraction Criteria
              And No Marker List in Extraction Criteria
              And No Marker Groups in Extraction Criteria
              And Submit button is red
                  
        @GE-176
        Scenario: Gherkin-byMarker_popup_Yes
              And select Dataset Type "dominant_non_nucleotide"
              And select Marker Group "Affy_dom_mg_1-25"
             Then Submit button is green
              And select List
             Then a popup 'Marker Groups Already Selected' appears
              And select Yes
              And List is selected
              And List textbox is empty
              And Submit button is red

        @GE-175
        Scenario: Gherkin-byMarker_popup_No
              And select Dataset Type "dominant_non_nucleotide"
              And select Marker Group "Affy_dom_mg_1-25"
              And select List
             Then a popup 'Marker Groups Already Selected' appears
              And select No
             Then Marker Group is selected
              And Marker Group "Affy_dom_mg_1-25" is checked
              And Submit button is green

        @GE-174
        Scenario: Gherkin-byMarker_popup
              And select Dataset Type "dominant_non_nucleotide"
              And select Marker Group "Affy_dom_mg_1-25"
              And select List
             Then a popup 'Marker Groups Already Selected' appears
              And states 'Marker Groups already selected. Specify Marker List instead?'
              And there is an option to select No or Yes

        @GE-206
        Scenario: Gherkin-byMarkers_listLimit 201
              And set list of names to "201" entries
             Then a popup appears with heading "Maximum Marker List Items Exceeded"
              And body reads "You attempted to paste more than 200 Marker List items; Please reduce the size of the list"

        @GE-211
        Scenario: Gherkin-byMarkers_listLimit 200
              And select Dataset Type "co_dominant_non_nucleotide"
              And set list of names to "200" entries
             Then Submit button is green
 
        @GE-180
        Scenario: byMarkers_minimumSelections
              And select Dataset Type "dominant_non_nucleotide"
             Then Submit button is red
             When select Platform "Affymetrix_Axiom"
             Then Submit button is green
 
        Scenario: Clicking Clear Button resets fields to default values
              And set Extract Filtering
                  | datasetType                | platform         | markerGroup      | format   | mapset  |
                  | co_dominant_non_nucleotide | Affymetrix_Axiom | Affy_dom_mg_1-25 | Flapjack | devtest |
              And user selects Clear button
             Then "Select a Dataset Type" is selected in Dataset Types
              And No Platforms selected
              And "Hapmap" is selected Format
              And "No Mapset" is selected Mapset
              And "Hapmap" is selected Format
             Then I see Extraction Criteria match
                  | format |
                  | Hapmap |
              And No Mapset in Extraction Criteria
              And No Dataset Type in Extraction Criteria
              And No Marker Criteria Platform in Extraction Criteria
              And No Marker File in Extraction Criteria
              And No Marker List in Extraction Criteria
              And No Marker Groups in Extraction Criteria
              And Submit button is red

        Scenario: Switching tabs resets fields to default values
              And set Extract Filtering
                  | datasetType                | platform         | markerGroup    | format   | mapset  |
                  | co_dominant_non_nucleotide | Affymetrix_Axiom | Affy_dom_mg_v2 | Flapjack | devtest |
              And I select By Dataset
              And I select By Markers
             Then "Select a Dataset Type" is selected in Dataset Types
              And No Platforms selected
              And "Hapmap" is selected Format
              And "No Mapset" is selected Mapset
              And "Hapmap" is selected Format
             Then I see Extraction Criteria match
                  | format |
                  | Hapmap |
              And No Mapset in Extraction Criteria
              And No Dataset Type in Extraction Criteria
              And No Marker Criteria Platform in Extraction Criteria
              And No Marker File in Extraction Criteria
              And No Marker List in Extraction Criteria
              And No Marker Groups in Extraction Criteria
              And Submit button is red
        
      